# Mattermost

Docker scripts that install and run
[Mattermost](https://mattermost.com/) in a container.

## Install

  - First install `ds`, `mariadb` and `revproxy`:
     + https://gitlab.com/docker-scripts/ds#installation
     + https://gitlab.com/docker-scripts/mariadb#installation
     + https://gitlab.com/docker-scripts/revproxy#installation

  - Then get the scripts: `ds pull mattermost`

  - Create a directory for the container: `ds init mattermost @mattermost.example.org`

  - Fix the settings: `cd /var/ds/mattermost.example.org/; vim settings.sh`

  - Create the container: `ds make`

For sending notification emails see:
https://gitlab.com/docker-scripts/postfix/-/blob/master/INSTALL.md
and: https://docs.mattermost.com/install/smtp-email-setup.html

To set up your push notification service see:
https://developers.mattermost.com/contribute/mobile/push-notifications/service/

## Other commands

```
ds mm help
```

For more details see:
https://docs.mattermost.com/administration/command-line-tools.html#mattermost

```
ds shell
ds stop
ds start
ds help
```