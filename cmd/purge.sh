cmd_purge_help() {
    cat <<_EOF
    purge
        Remove the container and the related data.

_EOF
}

cmd_purge() {
    ds remove
    ds revproxy rm
    ds mariadb drop
    rm -rf config/ data/ logs/ plugins/ client-plugins/
}
