cmd_mm_help() {
    cat <<_EOF
    mm [...]
        Run mattermost commands inside the container.

_EOF
}

cmd_mm() {
    if test -t 0 ; then
        docker exec -it \
               --user mattermost \
               $CONTAINER \
               env TERM=xterm \
               bin/mattermost "$@"
    else
        docker exec -i \
               --user mattermost \
               $CONTAINER \
               bin/mattermost "$@"
    fi
}
