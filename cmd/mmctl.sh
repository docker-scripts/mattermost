cmd_mmctl_help() {
    cat <<_EOF
    mmctl [...]
        Run mattermost commands inside the container.

_EOF
}

cmd_mmctl() {
    if test -t 0 ; then
        docker exec -it \
               --user mattermost \
               $CONTAINER \
               env TERM=xterm \
               bin/mmctl --local "$@"
    else
        docker exec -i \
               --user mattermost \
               $CONTAINER \
               bin/mmctl --local "$@"
    fi
}
