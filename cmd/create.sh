cmd_create_help() {
    cat <<_EOF
    create
        Create the container '$CONTAINER'.

_EOF
}

rename_function cmd_create orig_cmd_create
cmd_create() {
    mkdir -p config data logs plugins client-plugins
    orig_cmd_create \
        --mount type=bind,src=/etc/localtime,dst=/etc/localtime,readonly \
        --mount type=bind,src=$(pwd)/config,dst=/opt/mattermost/config \
        --mount type=bind,src=$(pwd)/data,dst=/opt/mattermost/data \
        --mount type=bind,src=$(pwd)/logs,dst=/opt/mattermost/logs \
        --mount type=bind,src=$(pwd)/plugins,dst=/opt/mattermost/plugins \
        --mount type=bind,src=$(pwd)/client-plugins,dst=/opt/mattermost/client/plugins \
        --workdir /opt/mattermost \
        "$@"
}
