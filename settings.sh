APP=mattermost
DOMAIN="mm.example.org"

ADMIN_USER=admin
ADMIN_EMAIL=admin@example.org
ADMIN_PASS=pass123

DBHOST=mariadb
DBPORT=3306
DBNAME=${DOMAIN//./_}
DBUSER=${DOMAIN//./_}
DBPASS=${DOMAIN//./_}
