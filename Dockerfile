include(bookworm)

### install some other packages
RUN <<EOF
  apt update
  apt upgrade --yes
  apt install --yes wget jq
EOF

RUN <<EOF
  # see: https://docs.mattermost.com/about/mattermost-server-releases.html
  wget -O mattermost.tar.gz \
       https://releases.mattermost.com/9.11.1/mattermost-9.11.1-linux-amd64.tar.gz
  tar -xzf mattermost.tar.gz
  rm mattermost.tar.gz
  mv mattermost* /opt/mattermost
  mv /opt/mattermost/config /opt/mattermost/config.orig
EOF
