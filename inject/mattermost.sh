#!/bin/bash -x

# load global settings
global_settings=$(dirname $0)/global_settings.sh
[[ -f $global_settings ]] && source $global_settings

# load local settings
source /host/settings.sh

main() {
    create_user_and_group
    setup_systemd_service

    # initialize the config file, if it does not exist
    [[ -f config/config.json ]] || { init_config_file; set_config; }

    systemctl restart mattermost
}

# create a user and group for mattermost
create_user_and_group() {
    useradd --system --user-group mattermost
    chown -R mattermost:mattermost /opt/mattermost
    chmod -R g+w /opt/mattermost
}

setup_systemd_service() {
    cat <<EOF > /lib/systemd/system/mattermost.service
[Unit]
Description=Mattermost
After=network.target

[Service]
Type=notify
ExecStart=/opt/mattermost/bin/mattermost
TimeoutStartSec=3600
KillMode=mixed
Restart=always
RestartSec=10
WorkingDirectory=/opt/mattermost
User=mattermost
Group=mattermost
LimitNOFILE=49152

[Install]
WantedBy=multi-user.target
EOF
    systemctl daemon-reload
    systemctl enable mattermost
}

init_config_file() {
    # create an initial config file
    local siteurl="https://$DOMAIN/"
    local datasource="$DBUSER:$DBPASS@tcp($DBHOST:3306)/$DBNAME?charset=utf8mb4,utf8&readTimeout=30s&writeTimeout=30s"
    cat config.orig/config.json \
        | jq '.ServiceSettings.EnableLocalMode = true' \
        | jq --arg siteurl "$siteurl" '.ServiceSettings.SiteURL = $siteurl' \
        | jq '.SqlSettings.DriverName = "mysql"' \
        | jq --arg datasource "$datasource" '.SqlSettings.DataSource = $datasource' \
             > config/config.json

    # make sure that ownership and permittions are correct
    chmod 600 config/config.json
    chown mattermost: config/config.json

    # start the service
    systemctl start mattermost
}

set_config() {
    # create a configuration script
    cat <<'EOF' > set_config.sh
#!/bin/bash

source settings.sh
cfgset='bin/mmctl --local --quiet config set'

# setup SMTP for sending emails
if [[ -n $SMTP_SERVER ]]; then
    $cfgset EmailSettings.SMTPServer "$SMTP_SERVER"
    $cfgset EmailSettings.SMTPPort 25
    $cfgset EmailSettings.ConnectionSecurity STARTTLS
    $cfgset EmailSettings.SMTPUsername ""
    $cfgset EmailSettings.SMTPPassword ""
    $cfgset EmailSettings.FeedbackName "$DOMAIN"
    $cfgset EmailSettings.FeedbackEmail "info@$SMTP_DOMAIN"
    $cfgset EmailSettings.ReplyToAddress "info@$SMTP_DOMAIN"
fi

# set some other email options
$cfgset EmailSettings.EnablePreviewModeBanner false
$cfgset EmailSettings.RequireEmailVerification true
$cfgset EmailSettings.SendEmailNotifications true
$cfgset EmailSettings.EnableEmailBatching true

$cfgset ServiceSettings.EnableMultifactorAuthentication true
$cfgset ServiceSettings.EnableLinkPreviews true
$cfgset ServiceSettings.EnableEmailInvitations true
$cfgset ServiceSettings.EnableSVGs true
$cfgset ServiceSettings.EnableLatex true

$cfgset FileSettings.EnablePublicLink true
$cfgset TeamSettings.EnableOpenServer true
$cfgset LogSettings.EnableConsole false

# create an admin user
bin/mmctl --local user create \
          --username $ADMIN_USER \
          --password "$ADMIN_PASS" \
          --email $ADMIN_EMAIL \
          --email-verified \
          --system-admin
EOF

    # run this script as user mattermost
    chmod +x set_config.sh
    cp /host/settings.sh .
    chown mattermost: settings.sh
    su mattermost -c ./set_config.sh

    # clean up
    rm settings.sh set_config.sh
}

# start the main function
main "$@"
